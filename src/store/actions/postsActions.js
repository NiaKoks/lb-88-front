import axios from '../../axios';
export const POSTS_SUCCESS = 'POSTS_SUCCESS';

export const getPostsSuccess = (forumpost) =>({type: POSTS_SUCCESS,forumpost});

export const getPosts =(id)=>{
    return dispatch =>{
        return axios.get(`/news/?${id}`)
                    .then(response => dispatch(getPostsSuccess(response.data)))
        }
};
export const postNewPost =()=>{
    return (dispatch) =>{
        const news = {
            title:'',
            text:'',
            date: '',
            img: null
        };
        axios.post('/',news).then(
            ()=> dispatch(getPosts())
        )
    }
};