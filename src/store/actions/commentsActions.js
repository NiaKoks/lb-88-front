import axios from '../../axios';
export const COMMENT_SUCCESS = 'COMMENT_SUCCESS';
export const getCommentSuccess = (comments)=>({type: COMMENT_SUCCESS, comments});

export const getComments = ()=>{
    return dispatch =>{return axios.get('/comments').then(
        response =>{dispatch(getCommentSuccess(response.data))
        }
    )}
};
export const sendComment = ()=>{
    return(dispatch)=>{
        const comment ={
            name: '',
            text: '',
        };
        axios.post('/comments',comment).then(()=> dispatch(getComments()))
    }
};
