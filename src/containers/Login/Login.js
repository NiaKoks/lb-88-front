import React, {Component,Fragment} from 'react';
import {connect} from "react-redux";
import {Form,FormGroup,Col,Button} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement"
import {loginUser} from "../../store/actions/userActions";
class Login extends Component {
    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.loginUser({...this.state});
    };

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };
    render() {
        return (
            <Fragment>
                <h2>Login</h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('username')}
                        placeholder="Enter your username"
                        autoComplete="current-username"
                    />
                    <FormElement
                        propertyName="password"
                        title="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('password')}
                        placeholder="Enter your password"
                        autoComplete="current-password"
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Login
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}
const mapStateToProps = state =>({
    error: state.users.loginError
});
const mapDispatchToProps = dispatch =>({
    loginUser: userData => dispatch(loginUser(userData))
});
export default connect(mapStateToProps,mapDispatchToProps) (Login);