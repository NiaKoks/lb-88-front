import React, {Component,Fragment} from 'react';
import PostCard from "../../components/PostCard/PostCard";

import {getPosts} from "../../store/actions/postsActions"
import {connect} from "react-redux";


class AllNews extends Component {
    componentDidMount() {
            const id = this.props.match.params.id;
            this.props.getPosts(id);
    }

    render() {
        return (
            <Fragment>
                <PostCard posts={this.props.forumposts}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state =>{
  return{
      forumposts: state.forumposts.forumposts
  }
};

const mapDispatchToProps = dispatch =>{
    return{
        getPosts: (id) => dispatch(getPosts(id))
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(AllNews);