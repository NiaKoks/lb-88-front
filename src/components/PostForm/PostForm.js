import React, {Component,Fragment} from 'react';
import {Form,FormGroup,Input,Label,Button} from "reactstrap";

class PostForm extends Component {

    state={
      title:" ",
      comment:" "
    };

    changeHandler = (event) =>{
        this.setState({
            [event.target.name] : event.target.value
        })
    };

    sendHandler =()=>{
      this.props.sendComment(this.state.title,this.state.comment)
    };

    render() {
        return (
            <Fragment>
                <Form className="AddComment" inline>
                    <Label for="title">Title:</Label>
                    <Input type="text"
                           value={this.state.title} name="title" id="title"
                           onChange={this.changeHandler} placeholder="title of post"
                    />
                </Form>
            </Fragment>
        );
    }
}

export default PostForm;