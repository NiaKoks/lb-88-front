import React from 'react';
import {Card, CardBody, CardImg} from "reactstrap";
import {Link} from "react-router-dom";


const PostCard = (props) => {
    return (
        props.forumposts ? props.forumposts.map((forumpost,index)=>{
            return(
                <Card style={{marginLeft: '10px',display:'inline-block'}}>
                    <CardBody>
                        <CardImg image={forumpost.image}/>
                        <Link to={'/post' + forumpost._id}/>
                    </CardBody>
                </Card>
            )
        }) : null
    );
};

export default PostCard;