import React, { Component,Fragment} from 'react';
import {Route,Switch,withRouter} from "react-router-dom";

import AllNews from "../src/containers/MainPage/AllNews"
import Toolbar from "../src/components/UI/Toolbar/Toolbar";
// import NewPost from "./containers/NewPost/NewPost";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register"
import {connect} from "react-redux";


class App extends Component {
  render() {
    return (
        <Fragment>
          <header><Toolbar user={this.props.user}/></header>
          <Switch>
            <Route path="/" exact component={AllNews}/>
            {/*<Route path="/forum_post/:id" exact component={ForumPost}/>*/}
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
          </Switch>
        </Fragment>
    );
  }
}

const mapStateToProps = state =>({
  user: state.users.user
});

export default  withRouter(connect(mapStateToProps)(App));